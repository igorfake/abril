window.app = window.app || {};

window.app.header = (function(){

    var header = document.getElementsByTagName("header")[0];
    var container = document.getElementsByClassName("header-container")[0];
    var fixed = false;

    function setHeaderPosition() {
        var offset = container.getBoundingClientRect();

        if(!fixed && offset.top <= 0) {
            fixed = true;
            header.className += " fixed";
        }

        if(fixed && offset.top > 0) {
            fixed = false;
            header.className = header.className.replace(/\s?fixed/, "");
        }
    }

    // On Scroll
    window.addEventListener("scroll", setHeaderPosition);

    return {
        el : header,
        container : container,
        fixed : fixed,
        setHeaderPosition : setHeaderPosition
    }

})();