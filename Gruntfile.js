module.exports = function(grunt) {

    var path = require('path');
    require('load-grunt-config')(grunt, {
        configPath: path.join(process.cwd(), 'tasks')
    });


    // Tasks
    grunt.task.registerTask('build', ['stylus', 'uglify']);

};