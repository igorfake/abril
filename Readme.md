# Abril

This a test project for Editora Abril.

## Dependencies
* Node 4+
* Grunt

## Install

```
npm install
```

## Build
Use `grunt watch` to build when a file change or `grunt build` to manualy build the project

## Run

```
npm start
```