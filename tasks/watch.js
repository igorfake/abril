module.exports = function (grunt, options) {
    return {
        stylus: {
            options: {
                livereload: true
            },
            
            files: ['css/src/**/*.styl'],
            tasks: ['stylus']
        },

        uglify: {
            options: {
                livereload: true
            },
            
            files: ['js/src/**/*.js'],
            tasks: ['uglify']
        },

        html: {
            options: {
                livereload: true
            },
            
            files: ['index.html']
        }
    };
};