var rupture = require("rupture");
var jeet = require("jeet");

module.exports = function (grunt, options) {
    return {
        dist : {
            options : {
                compress: true,
                paths: ["css/src/components"],

                // plugins
                use : [
                    function () {
                        return rupture({ implicit: true });
                    },

                    function () {
                        return jeet();
                    }
                ]
            },
            files: {
                "css/dist/global.css": "css/src/global.styl",
                "css/dist/home.css": "css/src/home.styl"
            }
        }
    };
};