module.exports = function (grunt, options) {
    return {
        dist : {
            options: {
                sourceMap: true
            },
            files: {
                'js/dist/global.min.js': ['js/src/global/app.js', 'js/src/global/**/*.js']
            }
        }
    };
};